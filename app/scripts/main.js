//Filip van Harreveld
// v.0
(function (window) {

	function Main() {
		console.log("d");
		drawLine();
	};

	function drawLine() {
		
		windowWidth = $(window).width() -20;
		windowHeight = $(window).height() - 20;
		$(window).resize(function() {
			windowWidth = $(window).width() -20;
			windowHeight = $(window).height() - 20;
		})
		
		var mouseX;
		var mouseY;


		var paper = Raphael("raphael-container", '100%', '100%');
		
		var bgRect = paper.rect(0,0,windowWidth,windowHeight);
		bgRect.attr({fill: "black", stroke: "none"});

		//lines
		var lineBR = paper.path("M0 0L0, 0");
		var lineTL = paper.path("M30 0L0, 0");
		var lineBL = paper.path("M30 0L0, 0");
		var lineTR = paper.path("M30 0L0, 0");

		lineTL.attr({
			'stroke-width': 0.3,
			'stroke': "white"
		});

		lineBR.attr({
			'stroke-width': 0.3,
			'stroke': "white"
		});		
		lineBL.attr({
			'stroke-width': 0.3,
			'stroke': "white"
		});		
		lineTR.attr({
			'stroke-width': 0.3,
			'stroke': "white"
		});
		//end lines

		var S1_1;
		var S1_2;
		var S1_3;
		var S1_4;

		var S2_1;
		var S2_2;
		var S2_3;
		var S2_4;


		var S3_1;
		var S3_2;
		var S3_3;
		var S3_4;

		var S4_1;
		var S4_2;
		var S4_3;
		var S4_4;

		var S5_1;
		var S5_2;
		var S5_3;
		var S5_4;


		var speed = 0;
		var ease = 'linear';

		var mousehitArea = paper.rect(0,0,windowWidth,windowHeight);
		mousehitArea.attr({'fill': 'black','fill-opacity':0});
		function randNumber(_min,_max) {
			var rand = Math.floor((Math.random()*_max)+_min);
			return rand;
		}
		var i1 = randNumber(80,100);
		var i2 = randNumber(180,200);
		var i3 = randNumber(280,300);
		var i4 = randNumber(380,400);
		var i5 = randNumber(500,550);



		var loops = [
			{loop:S1_1,diameter:i1,start:lineBR,end:lineTR,xCurve:-10,yCurve:0},
			{loop:S1_2,diameter:i1,start:lineTR,end:lineTL,xCurve:100,yCurve:20},
			{loop:S1_3,diameter:i1,start:lineTL,end:lineBL,xCurve:10,yCurve:0},
			{loop:S1_4,diameter:i1,start:lineBL,end:lineBR,xCurve:-100,yCurve:-10},
//
			{loop:S2_1,diameter:i2,start:lineBR,end:lineTR,xCurve:-10,yCurve:0},
			{loop:S2_2,diameter:i2,start:lineTR,end:lineTL,xCurve:100,yCurve:20},
			{loop:S2_3,diameter:i2,start:lineTL,end:lineBL,xCurve:10,yCurve:0},
			{loop:S2_4,diameter:i2,start:lineBL,end:lineBR,xCurve:-100,yCurve:-10},
//
			{loop:S3_1,diameter:i3,start:lineBR,end:lineTR,xCurve:-10,yCurve:0},
			{loop:S3_2,diameter:i3,start:lineTR,end:lineTL,xCurve:100,yCurve:20},
			{loop:S3_3,diameter:i3,start:lineTL,end:lineBL,xCurve:10,yCurve:0},
			{loop:S3_4,diameter:i3,start:lineBL,end:lineBR,xCurve:-100,yCurve:-10},
//
			{loop:S4_1,diameter:i4,start:lineBR,end:lineTR,xCurve:-10,yCurve:0},
			{loop:S4_2,diameter:i4,start:lineTR,end:lineTL,xCurve:100,yCurve:20},
			{loop:S4_3,diameter:i4,start:lineTL,end:lineBL,xCurve:10,yCurve:0},
			{loop:S4_4,diameter:i4,start:lineBL,end:lineBR,xCurve:-100,yCurve:-10},
//
			{loop:S5_1,diameter:i5,start:lineBR,end:lineTR,xCurve:-10,yCurve:0},
			{loop:S5_2,diameter:i5,start:lineTR,end:lineTL,xCurve:100,yCurve:20},
			{loop:S5_3,diameter:i5,start:lineTL,end:lineBL,xCurve:10,yCurve:0},
			{loop:S5_4,diameter:i5,start:lineBL,end:lineBR,xCurve:0,yCurve:0}

		];
		

		var loopLength = loops.length;


		//generate lines
		for (var i=0;i<loopLength;i++) {
			
			loops[i].loop = paper.path("M30 0L0, 0"); 
			loops[i].loop.attr({
				'stroke-width': 0.5+i/20,
				'stroke': "white"
			});
		}



		mousehitArea.mousemove(function (e) {
			mouseX = e.clientX;
			mouseY = e.clientY;
		});
//



			/*
			lineBL.animate({"stroke-width":stokeWidthCalLeft(mouseX) },20);
			lineTL.animate({"stroke-width":stokeWidthCalLeft(mouseX) },20);
			lineBR.animate({"stroke-width":stokeWidthCalRight(mouseX) },20);			
			lineTR.animate({"stroke-width":stokeWidthCalRight(mouseX) },20);
*/

			window.requestAnimFrame = (function(){
			return  window.requestAnimationFrame       ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			function( callback ){
			window.setTimeout(callback, 1000 / 60);
			};
			})();
			window.setTimeout(function() {
				(function animloop(){
				requestAnimFrame(animloop);
				render();
			})();

			},800)

			function render(){
			lineBR.animate({path:"M"+mouseX+" " +mouseY+"L"+windowWidth + "," + windowHeight},speed,ease);
			lineTR.animate({path:"M"+mouseX+ " " +mouseY+"L"+windowWidth + "," + 0},speed,ease);
			lineTL.animate({path:"M"+mouseX+" " +mouseY+"L"+0 + "," + 0},0);
			lineBL.animate({path:"M"+mouseX+" " +mouseY+"L"+0 + "," + windowHeight},speed,ease);

			for (var i=loopLength;i--;) {
				var loop = loops[i];
				var loop1 = loop.diameter;
				
				var start = loop.start;
				var end = loop.end;

				var xCurve = loop.xCurve;
				var yCurve = loop.yCurve;

				loops[i].loop.animate({
					path:"M"
					+(start.getPointAtLength(loop1).x)
					+ " " 
					+start.getPointAtLength(loop1).y
					+"S"
					+(end.getPointAtLength(loop1).x + xCurve)
					+ " " 
					+( end.getPointAtLength(loop1).y + yCurve)
					+ " "
					+(end.getPointAtLength(loop1).x)
					+ " " 
					+(end.getPointAtLength(loop1).y)
				},speed,ease);	
			}
		}

//
	};

	function stokeWidthCalLeft (_mouseX) {
		var cal = 5-(_mouseX/200);
		if (cal < 0.5){
			cal = 0.5;
		}
		return cal;
	}
	
	function stokeWidthCalRight (_mouseX) {
		var cal = 0+(_mouseX/300);
		if (cal < 0.5){
			cal = 0.5;
		}
		return cal;
	}

window.Main = Main;
$(document).ready(function(){
	Main();
});

}(window));